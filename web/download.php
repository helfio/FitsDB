<!--    FitsDB extrae información de las cabeceras de archivos FITS,
    las almacena en una base de datos y las pone a disposición 
    del usuario vía web.
    Copyright (C) 2015  Juan Pablo Navarro Sánchez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.-->
    
<?php
set_time_limit(0);
// session_start();
// $files = array();
// $files = file('sesion/fitsdb_' . session_id());
// $files = array('23dec12_017.fit','ImagenesPrueba/2003EL61-010R.fit');

$files = ($_POST['selector']);
if (!file_exists('descargas/')){
  mkdir('descargas/',0777);}

if(!file_exists('descargas/'.date('d'))){
  mkdir('descargas/' . date('d') ,0777);
}
$path = 'descargas/' . date('d') . '/';




if ($_POST['descargazip']){
  $zip = new ZipArchive();
  $num = 0;
  $zipname = 'archivosfits' . $num . '.zip';

  while (file_exists($path . $zipname)){
    $num++;
    $zipname = 'archivosfits' . $num . '.zip';
  }
  $zip->open($path . $zipname, ZipArchive::CREATE);
  foreach ($files as $file) {
  $bueno = trim($file);
    if (is_readable($bueno)){
      $lista = explode('/',$file);
      $p = 3; //Profundidad
      $n = count($lista);
      $nuevo = '';
      for ($j=$p;$j>0;$j--){
	$nuevo= $nuevo.'/'.$lista[$n-$j];
      }
      $zip->addFile($bueno,$nuevo);
    }
  }
  $zip->close();
  header('Content-Type: application/zip');
  header('Content-disposition: attachment; filename=' . $zipname);
  //$size = filesize('archivosfits.zip');
  //header('Content-Length: ' . $size);

  readfile($path . $zipname);
}

// if ($_POST['descargatar']){
//   $num = 0;
//   $tarname = 'archivosfits' . $num . '.tar';
// 
//   while (file_exists($path . $tarname)){
//     $num++;
//     $tarname = 'archivosfits' . $num . '.tar';
//   }
//   $tar = new PharData($tarname);
//   foreach ($files as $file) {
//   $bueno = trim($file);
//     if (is_readable($bueno)){
//       $tar->addFile($bueno);
//     }
//   }
//   header('Content-Type: application/tar');
//   header('Content-disposition: attachment; filename=' . $tarname);
//   readfile($path . $tarname);
// }

?>

