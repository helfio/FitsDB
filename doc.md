<!--    FitsDB extrae información de las cabeceras de archivos FITS,
    las almacena en una base de datos y las pone a disposición 
    del usuario vía web.
    Copyright (C) 2015  Juan Pablo Navarro Sánchez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.-->

# Documentación
A continuación se describe la documentación relativa al software desarrollado por Juan Pablo Navarro Sánchez para el manejo y consulta de datos fits en base de datos.

Actualmente el proyecto consta de las siguientes utilidades:

- extracampos.py
- otros

Pasamos ahora a describir el funcionamiento y los pormenores de cada utilidad.

## extracampos.py

### Funcionamiento
Se encarga de extraer los nombres de los campos de las cabeceras (headers) de cada uno de los archivos fits/fit/fts y evitando duplicidades los reune en un archivo llamado *nombres_de_campos* que se genera al concluir su ejecución en el mismo directorio de ejecución. Además se muestra el número de imágenes procesadas.

Esta utilidad admite como único argumento el directorio donde se encuentran las imágenes. Si se ejecuta sin argumentos o con más de uno, se mostrará un mensaje de error indicando cómo debe ejecutarse.

### Funciones

- CheckFileExistence(nombrearchivo)
- Add(url,salida)
- Sort(archivo)
- HashFile(ruta)
- GenCsvWithHeaders(sitio,name)

#### CheckFileExistence(nombrearchivo)
Comprueba si el archivo que recibe como argumento existe. Si es así devuelve 1, y si no es así primero crea el archivo y luego devuelve 0.

#### AddCampos(url,salida)
Añade los campos del archivo que se encuentra en *url* al archivo que se encuentra en *salida*.

#### Sort(archivo)
Ordena el archivo que se le da como argumento por orden alfabético.

#### HashFile(ruta)
Genera la suma de verificación md5 del archivo que se le da como argumento.

#### GenCsvWithHeaders(sitio,name)
POR COMPLETAR
Genera un archivo csv (";" como separadores) con los datos listos para incorporar a la base de datos.

#### JD2Date(entrada)
Genera una fecha y hora en formato legible a partir de una fecha en formato de día juliano.

#### MJD2Date(entrada)
Genera una fecha y hora en formato legible a partir de una fecha en formato de día juliano modificado.

### Librerías

- os
- sys
- pyfits
- hashlib
- termcolor

#### os 
Se utiliza para el trabajo con archivos.

#### sys
Se utiliza para interactuar con el sistema.

#### pyfits
Se utiliza para trabajar de forma más cómoda con los archivos fits.

#### hashlib
Se utiliza para calcular sumas de verificación.

#### termcolor
Se utiliza para mostrar salidas en color por pantalla.