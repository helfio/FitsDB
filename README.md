    FitsDB extrae información de las cabeceras de archivos FITS,
    las almacena en una base de datos y las pone a disposición 
    del usuario vía web.
    Copyright (C) 2015  Juan Pablo Navarro Sánchez

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# FitsDB
Originalmente diseñada para una base de datos de imágenes de *Objetos Transneptunianos y Centauros*. Es un utilidad que extrae información de las cabeceras de archivos de formato FITS, la almacena en una base de datos y los pone a disposición del usuario a través de una intefaz web para su descarga.

Los archivos .fits corresponen a imágenes de observaciones astronómicas, y las colecciones de este tipo de archivos llegan a alcanzar un tamaño más que considerable.

La principal comodidad que ofrece esta utilidad es el acceso a imágenes concretas (según objeto observado, fecha, observatorio, filtro, etc) prácticamente de forma instantánea frente al tiempo que se tarda en hacer una búsqueda manual abriendo en cada caso primero la imagen y luego las cabeceras.

Además, utiliza el buscador del proyecto [Horizons](http://ssd.jpl.nasa.gov/horizons.cgi) del JPL de la NASA para generar búsquedas cruzadas con los tres tipos de nombres que reciben los Objetos Transneptunianos y Centauros. Estas búsquedas se almacenan en caché para preservar el ancho de banda del proyecto Horizons.

FitsDB cuenta con soporte para SQLite3 y MySQL, y un asistente de instalación para Ubuntu (testeado sobre la versión server 64bits 14.04).

---
# Antes de instalar

FitsDB consta de dos partes: una que escanea los directorios `fitsdb.py` y otra que facilita el acceso a la base de datos y a las imágenes (interfaz web).

La parte que escanea los directorios está escrita en *Python 2*.

La interfaz web no es más que dos archivos *php*, pero requieren un servidor web instalado en la máquina. El asistente de instalación utiliza *Lighttpd*, pero debería valer cualquier otro.

Además, el asistente utiliza *crontab* para programar un escaneo semanal del directorio donde se encuentran las imágenes. Manualmente se pueden programar escaneos adicionales tanto en tiempos como directorios distintos.

# Instalación

Si su distribución es **Ubuntu** ejecute el asistente.


Si utiliza cualquier otra distribución tendrá que hacer la instalación de forma manual.

## Asistente de instalación para Ubuntu

**ATENCIÓN: si ya tiene una instalación de *Lighttpd* en su sistema, y ha personalizado la configuración, absténgase de ejecutar el asistente; podría dañar su instalación de *Lighttpd*.**

Para iniciar el asistente abra un terminal en la carpeta donde está el repositorio clonado, y a continuación ejecute en un terminal:

`./ubuntu_install.sh`

El asistente le guiará por todos sus pasos y le pedirá los datos necesarios.

Una vez concluido el asistente, el FitsDB debería estar correctamente instalado en

`/home/$USER/.FitsDB`

Dentro de esta carpeta podrá encontrar varios archivos clave:

- **fitsdb.py**: Se encarga de escanear los archivos y añadirlos a la base de datos. Se ejecutará con la periodicidad configurada en el asistente sobre el directorio indicado.
- **logging.log**: Log rotatorio de *fitsdb.py*.
- **config.conf**: configuración de FitsDB; tanto del escaneador como de la interfaz web. En este archivo se encuentran los datos necesarios para que el programa funcione. **Editar con precaución.**
- **lighttpd_password**: Si ha elegido proteger con contraseña el acceso web, aquí podrá encontrar el usuario y contraseña que haya configurado. También puede añadir tantos como necesite con el formato: usuario:contraseña.
- Si elijió SQLite como base de datos durante la instalación, además habrá un archivo con extensión .db que es donde se guardan los datos generados por el escaneador. Si en cambio elijió MySQL, los datos los guarda el servidor y el archivo .db no aparecerá.

La interfaz web se encuentra en:

`/var/www/fitsdb`

En ella están los archivos de la interfaz, además de un archivo .db (si elijió utilizar SQLite) donde se almacena la caché de nombres de objetos.


## Instalación manual (En desarrollo)

**Este apartado se encuentra desactualizado; si desea instalar manualmente, ahora mismo su mejor opción es abrir con un editor el instalador de ubuntu y adaptar los pasos manualmente a su distribución.**

- Instalar las librerías que utiliza: MySQL-python, astropy, configparser,
mysql-connector-python, numpy, pyfits, termcolor
que se pueden instalar fácilmente con la herramienta 'pip'.
- Tener en marcha MySQL en el ordenador que vayáis a usar, con un usuario
para esto y una base de datos propia de ese usuario. Estos datos hay que
introducirlos en el archivo 'fitsdb.cfg' que se puede crear a partir de
'config.cfg.new'.
- Cuando vayáis a ejecutar el fitsdb.py hay que tener en cuenta que corre
con python 2.7 y que hay que pasarle como argumento el directorio raíz
donde se encuentran los archivos fits/fit.

- php5-sqlite
